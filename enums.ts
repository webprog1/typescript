enum CadinalDirections {
    North = "North",
    East = "East",
    South = "South",
    West = "West"
}

let CadinalDirection = CadinalDirections.East;

console.log(CadinalDirection)